import multer from "multer";

const errorHandler = (err, req, res, next) => {
  if (err instanceof multer.MulterError) {
    return res.status(400).json({ error: err.message });
  } else {
    return res
      .status(err.status || 500)
      .json({ error: err.message || "Internal server error" });
  }
};

export default errorHandler;
