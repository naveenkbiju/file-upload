import path from "path";
import sharp from "sharp";
import fs from "fs";

const uploadController = async (req, res, next) => {
  try {
    if (!req.files || req.files.length === 0) {
      const error = new Error("No files uploaded.");
      error.status = 400;
      throw error;
    }

    const files = [];

    for (const file of req.files) {
      const { filename, destination } = file;
      const oldFilePath = path.resolve(destination, filename);
      const newFilePath = path.resolve(destination, "resized-" + filename);
      sharp.cache(false);
      await sharp(oldFilePath).resize(500).toFile(newFilePath);
      fs.unlinkSync(oldFilePath);
      const { size, mimetype } = file;
      const filePath = `http://localhost:8000/resized-${filename}`;
      files.push({
        filePath,
        fileName: filename,
        size,
        type: mimetype,
      });
    }
    return res.status(200).json({
      message: "Image uploaded successfully.",
      files,
    });
  } catch (error) {
    next(error);
  }
};

export default uploadController;
