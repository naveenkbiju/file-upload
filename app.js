import express from "express";
import dotenv from "dotenv";
dotenv.config();
import uploadController from "./controllers/uploadController.js";
import upload from "./middlewares/fileUpload.js";
import errorHandler from "./middlewares/errorHandler.js";

const app = express();
const router = express.Router();
const port = process.env.PORT || 8000;
app.use(express.static("uploads"));

router.post("/upload", upload.array("file", 5), uploadController);

router.use(errorHandler);

app.use(router);

app.listen(port, () => {
  console.log(`Server is running at port: ${port}`);
});
